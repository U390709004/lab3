public class FindPrimes {

    public static void main(String[] args) {
        int range = Integer.parseInt(args[0]);

        System.out.print("2");
        for (int i = 3; i <= range; i++) {
            if (primeCheck(i)) {
                System.out.print("," + i);
            }

        }
    }

    public static boolean primeCheck(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;

    }
}
